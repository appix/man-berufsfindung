$(document).ready(function() {

	$('#browser-links').on('click', 'li', loadUrl);
	$('#video-links').on('click', 'li', loadVideo);

	function loadUrl(ev) {
		ev.preventDefault();
		$('#browser-links li').removeClass('active');
		$(this).addClass('active');

		$('#browser-document').attr('src', this.dataset.url);

		$('#browser-document').on('load', function() {
			var doc = $(document.getElementById('browser-document').contentDocument);
			/*doc.find('a').on('click', function(ev) {
				ev.preventDefault();
				if(checkUrl(this.href)) {
					$('#browser-document').attr('src', this.href);
				} else {
					alert("Dieser Link ist gesperrt");
				}
			});*/
		});
	}

	function checkUrl(url) {
		var regxp = /(mandieselturbo.com|tecmania.ch|yousty.ch)/i;
		return url.search(regxp) !== -1;
	}

	function loadVideo(ev) {
		ev.preventDefault();
		$('#video-links li').removeClass('active');
		$(this).addClass('active');

		$('#player').attr('src', 'videos/' + this.dataset.video);
	}

});
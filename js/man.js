var TouchKeyboard = (function () {

	var ks,
		activeInput,
		keyboardElement,
		keyShift = false,
		keyboardFragment = document.createDocumentFragment(),
		specialKeys = {
			space: [' ', spaceFunc],
			spacer: [''],

			// tab: [String.fromCharCode(8633)],
			// shift: [String.fromCharCode(8679), shiftFunc],
			// enter: [String.fromCharCode(8629)],
			// backspace: [String.fromCharCode(8592), backspaceFunc],

			tab: ['\u21B9'],
			shift: ['\u21E7', shiftFunc],
			enter: ['\u21B5'],
			backspace: ['\u2190', backspaceFunc],
		};

	var keyboarLayouts = {
		standard:
			[
				[1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 'backspace'],
				['q', 'w', 'e', 'r', 't', 'z', 'u', 'i', 'o', 'p', 'ü'],
				['a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'ö', 'ä'],
				['spacer', 'y', 'x', 'c', 'v', 'b', 'n', 'm', ',', '.'],
				['space']
			],
		email:
			[
				[1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 'backspace'],
				['q', 'w', 'e', 'r', 't', 'z', 'u', 'i', 'o', 'p', 'ü'],
				['a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'ö', 'ä'],
				['spacer', 'y', 'x', 'c', 'v', 'b', 'n', 'm', '.', '-', '_', '@']
			]
	};

	document.addEventListener('DOMContentLoaded', init, false);
	document.addEventListener('focus', setActiveFormElement, true);
	//For IE
	// document.onfocusin = setActiveFormElement;

	function setActiveFormElement(ev) {

		if(ev.target.nodeName == 'INPUT') {
			activeInput = ev.target;
		}
	}

	function init() {
		keyboardElement = document.getElementById('keyboard');
		// keyboardElement.addEventListener('touchend', keyhit, false);
		// keyboardElement.addEventListener('touchstart', lockEvent, false);
		// keyboardElement.addEventListener('touchmove', lockEvent, false);
		keyboardElement.addEventListener('click', keyhit, false);
		keyboardElement.addEventListener('transitionend', removeActive, true);
	}

	function keyhit(ev) {
		lockEvent(ev);
		if(ev.target.nodeName === 'LI') {
			var pressedKey = ev.target;
			pressedKey.classList.add('active');
			//if(pressedKey.dataset.letter) {
			if(pressedKey.getAttribute('data-letter')) {
				ev.preventDefault();
				if(activeInput.dataset.keyboardLayout == 'email' || activeInput.type != 'text') {
					//addChar(pressedKey.dataset.letter);
					addChar(pressedKey.getAttribute('data-letter'));
				} else {
					//addCharCapitalized(pressedKey.dataset.letter);
					addCharCapitalized(pressedKey.getAttribute('data-letter'));
				}

			} else {
				//specialKeys[pressedKey.dataset.key][1]();
				specialKeys[pressedKey.getAttribute('data-key')][1]();
			}
		}
	}

	function removeActive(ev) {
		if(ev.propertyName == 'background-color') {
			ev.target.classList.remove('active');
		}
	}

	function addChar(c) {
		activeInput.focus();
		var val = activeInput.value;
		var newVarA = val;
		var newVarB = '';
		try {
			newVarA = val.slice(0, activeInput.selectionStart);
			newVarB = val.slice(activeInput.selectionEnd);
		} catch(e) {
			//Chrome doesn't support selection on inputs with type=email
			// => simply append the letter
		}

		activeInput.value = newVarA + c + newVarB;
		var position = newVarA.length + 1;
		try {
			activeInput.setSelectionRange(position, position);
			if(activeInput.selectionEnd <= position) {
				activeInput.scrollLeft = activeInput.scrollLeft + 4;
			}
		} catch(e) {
			//Chrome doesn't support selection on inputs with type=email
		}
	}

	function addCharCapitalized(c) {
		activeInput.focus();
		var val = activeInput.value;
		var newVarA = val;
		var newVarB = '';
		try {
			newVarA = val.slice(0, activeInput.selectionStart);
			newVarB = val.slice(activeInput.selectionEnd);
		} catch(e) {
			//Chrome doesn't support selection on inputs with type=email
		}

		if(newVarA.length === 0) {
			c = c.toUpperCase();
		} else {
			if(newVarA.charAt(newVarA.length -1) === ' ') {
				c = c.toUpperCase();
			}
		}

		activeInput.value = newVarA + c + newVarB;
		var position = newVarA.length + 1;
		try {
			activeInput.setSelectionRange(position, position);
			if(activeInput.selectionEnd <= position) {
				activeInput.scrollLeft = activeInput.scrollLeft + 4;
			}
		} catch(e) {
			//Chrome doesn't support selection on inputs with type=email
		}
	}

	function spaceFunc() {
		addChar(' ');
	}

	function lockEvent(ev) {
		ev.preventDefault();
		ev.stopPropagation();
	}

	function backspaceFunc() {
		activeInput.focus();
		var val = activeInput.value;
		var cut = val.length;
		var selectionEnd = val.length;

		try {
			cut = activeInput.selectionStart;
			selectionEnd = activeInput.selectionEnd;
		} catch(e) {
			//Chrome doesn't support selection on inputs with type=email
		}
		if(cut === selectionEnd) cut--;
		var newVarA = val.slice(0, cut);
		var newVarB = val.slice(selectionEnd);
		activeInput.value = newVarA + newVarB;
		var position = newVarA.length;
		try {
			activeInput.setSelectionRange(position, position);
		} catch(e) {
			//Chrome doesn't support selection on inputs with type=email
		}

	}

	function shiftFunc() {
		activeInput.focus();
		keyShift = !keyShift;
		for(i = 0, ilen = ks.length; i < ilen; i++) {
			var kr = ks[i];
			for(j = 0, jlen = kr.length; j < jlen; j++) {
				var sk = specialKeys[kr[j]];
				if(sk) {
				} else {
					if(typeof kr[j] === 'string') {
						if(keyShift) {
							kr[j] = kr[j].toUpperCase();
						} else {
							kr[j] = kr[j].toLowerCase();
						}
					}
				}
			}
		}
		render();
	}


	function render() {

		var kbrd = document.createElement('ol');

		for(i = 0, ilen = ks.length; i < ilen; i++) {
			var kr = ks[i];
			var rli = document.createElement('li');
			var row = document.createElement('ol');
			rli.appendChild(row);

			for(j = 0, jlen = kr.length; j < jlen; j++) {
				var item = document.createElement('li');
				var keyName = kr[j];
				var sk = specialKeys[keyName];
				if(sk) {
					//item.dataset.key = keyName;
					item.setAttribute('data-key', keyName);
					item.appendChild(document.createTextNode(sk[0]));
					if(keyName === 'shift') {
						if(keyShift) item.classList.add('active');
					}
				} else {
					//item.dataset.letter = keyName;
					item.setAttribute('data-letter', keyName);
					item.appendChild(document.createTextNode(keyName));
				}

				row.appendChild(item);
			}

			kbrd.appendChild(rli);
		}

		keyboardFragment.appendChild(kbrd);

		if(keyboardElement.hasChildNodes()) {
			keyboardElement.replaceChild(keyboardFragment, keyboardElement.children[0]);
		} else {
			keyboardElement.appendChild(keyboardFragment);
		}
	}

	function setLayout(keys) {
		ks = keys;
		render();
	}

	return {
		setLayout: function (keys) {
			if(typeof keys === 'string') {
				setLayout(keyboarLayouts[keys]);
			} else {
				setLayout(keys);
			}
		}


	};

})();

var ManForm = {};

document.addEventListener('focus', loadKeyboard, true);
// document.onfocusin = loadKeyboard;
document.addEventListener('blur', closeKeyboard, true);

function loadKeyboard(ev) {
	if(ev.target.form){
		var elm = ev.target;
		var showKeyboard = true;
		switch(elm.type) {
			case 'email':
				TouchKeyboard.setLayout('email');
				break;
			/*case 'number':
				TouchKeyboard.setLayout([[7,8,9],[4,5,6],[1,2,3]]);
				break;*/
			case 'checkbox':
			case 'submit':
			case 'radio':
				showKeyboard = false;
				break;
			default:
				TouchKeyboard.setLayout(elm.dataset.keyboardLayout || 'standard');
		}
		if(showKeyboard) document.getElementById('keyboard').classList.add('opened');
	}
}

function closeKeyboard(ev) {
	setTimeout(function () {
		// if(document.activeElement == document.body) {
			// document.getElementById('keyboard').classList.remove('opened');
		// }
		var keyboardElement = document.getElementById('keyboard');
		if(document.activeElement != keyboardElement
		&& document.activeElement.tagName.toLowerCase() != 'input'
		&& document.activeElement.tagName.toLowerCase() != 'textarea' ) {
			keyboardElement.classList.remove('opened');
		}
	}, 300);

}

$(document).ready(function () {
	$('form').on('submit', submitForm);
	$('form').on('change', 'input', function (ev) {
		check_validity(this);
		if(this.type == 'checkbox') {
			var checked = $('input[name="' + this.name +'"]').filter(function (index) {
				return this.checked;
			});

			if(checked.length) {
				$(this).parents('ol').removeClass('invalid');
			} else {
				$(this).parents('ol').addClass('invalid');
			}
		}
	});

	function submitForm(ev) {

		ev.preventDefault();
		ev.stopPropagation();
		var data = $(this)[0].elements;
		var data_serialized = $(this).serialize();
		var form_is_valid = true;

		form_is_valid = check_validity(data.prename);
		form_id_valid = check_validity(data.name) && form_is_valid;

		if(data_serialized.indexOf('school%5B%5D') === -1) {
			form_is_valid = false;
			$('form ol')[0].classList.add('invalid');
		}

		if (data_serialized.indexOf('job%5B%5D') === -1) {
			form_is_valid = false;
			$('form ol')[1].classList.add('invalid');
		}

		if(!check_validity(data.email) && !check_validity(data.tel)){
			form_is_valid=false;
		}

		if(!check_validity(data.email) && check_validity(data.tel)){
			$(data.email).removeClass('invalid');


		}else if(check_validity(data.email) && !check_validity(data.tel)){

			$(data.tel).removeClass('invalid');


		}

		if(form_is_valid) {
			$.post('http://server.appix.ch/man/saveData.php', data_serialized, submitHandler, 'text');
		}else{
			$('#mail_error').show();

		}
	}

	function check_validity(elm) {
		if (!elm.validity.valid) {
			elm.classList.add('invalid');
			return false;
		}
		elm.classList.remove('invalid');
		return true;
	}

	function submitHandler(data, status) {
		if(!data) {
			$('form').hide();
			$('#formsend-confirm').show();
		}
	}

	ManForm.reset = function() {
		var form = $('form');
		form[0].reset();
		form.show();
		$('.invalid').removeClass('invalid');
		$('#formsend-confirm').hide();
	};

});var ManMap = {
  reset: function(){},

  showLinks: function(){}
};

function initialize() {
  var mapCenter = new google.maps.LatLng(47.416821,8.628702);
  var mapZoom = 11;
  var mapOptions = {
    zoom: mapZoom,
    minZoom: 8,
    center: mapCenter,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    streetViewControl: false,
    draggable: false,
    panControl: false,
    zoomControl: false,
    mapTypeControl: false,
    scrollwheel: false,
    disableDoubleClickZoom: true
  };

  var map = new google.maps.Map(document.getElementById('map-canvas'),
      mapOptions);

  $('#map-places').on('click', 'li', toggleMarkerInfo);

  var mapPlaces = {
    man: {
      title: 'man',
      content_title: "MAN Energy Solutions Schweiz AG",
      position: new google.maps.LatLng(47.390656, 8.521732),
      address:"Hardstrasse 319<br>8005 Zürich"
    },

    azw: {
      title: 'azw',
      content_title: "azw Ausbildungszentrum Winterthur",
      position: new google.maps.LatLng(47.497635, 8.716978),
      address: "Zürcherstrasse 25<br>8401 Winterthur"
    }
  };

  var infowindow = null;

  function drawMarkers() {
    for(var prop in mapPlaces) {
      if(mapPlaces.hasOwnProperty(prop)) {
        var marker_data = mapPlaces[prop];

        marker_data.marker = new google.maps.Marker({
          position: marker_data.position,
          map: map,
          title: marker_data.title
        });

        marker_data.infowindow = new google.maps.InfoWindow({
          content: '<div id="content">'+
              '<div id="siteNotice">'+
              '</div>'+
              '<h1 id="firstHeading" class="firstHeading">'+marker_data.content_title+'</h1>'+
              '<div id="bodyContent">'+
              '<address>'+marker_data.address+'</address>'+
              '</div>'+
              '</div>',
          maxWidth: 200
        });

        google.maps.event.addListener(marker_data.marker, 'click', toggleInfo);

      }
    }
  }

  drawMarkers();

  function showAllMarkers() {
    for(var prop in mapPlaces) {
      if(mapPlaces.hasOwnProperty(prop)) {
        var marker_data = mapPlaces[prop];
        marker_data.marker.setVisible(true);
      }
    }
  }

  function toggleInfo() {
    if(infowindow) {
      $('li[data-marker]').removeClass('active');
      infowindow.close();
    }
    infowindow = mapPlaces[this.title].infowindow;
    infowindow.open(map,mapPlaces[this.title].marker);
    $('li[data-marker="' + this.title + '"]').addClass('active');
    google.maps.event.addListenerOnce(infowindow, 'closeclick', resetWithoutClose);
    map.panTo(mapPlaces[this.title].position);
    map.setZoom(13);

  }

  function toggleMarkerInfo(ev) {
    if($(this).hasClass('active')) {
      reset();
    } else {
      //if(this.dataset.marker) {
      //  toggleInfo.call(mapPlaces[this.dataset.marker]);
      //}
      if(this.getAttribute('data-marker')) {
        toggleInfo.call(mapPlaces[this.getAttribute('data-marker')]);
      }
		//google.maps.event.trigger(map, 'resize');
    }
  }

  function reset() {
	//google.maps.event.trigger(map, 'resize');
    map.setCenter(mapCenter);
    map.setZoom(mapZoom);
    if(infowindow) {
      $('li[data-marker]').removeClass('active');
      infowindow.close();
    }
  }

  function resetMap() {
    reset();
    showLinks();
  }

  function resetWithoutClose() {
    map.setCenter(mapCenter);
    map.setZoom(mapZoom);
    $('li[data-marker]').removeClass('active');
	//google.maps.event.trigger(map, 'resize');
  }

  function showLinks() {
	//reset();
    if(arguments.length !== 0) {
      if(!arguments[0]) {
        $('#map-places li').eq(0).hide();
        //mapPlaces[$('#map-places li')[0].dataset.marker].marker.setVisible(false);
        mapPlaces[$('#map-places li')[0].getAttribute('data-marker')].marker.setVisible(false);
      }

      if(!arguments[1]) {
        $('#map-places li').eq(1).hide();
        //mapPlaces[$('#map-places li')[1].dataset.marker].marker.setVisible(false);
        mapPlaces[$('#map-places li')[1].getAttribute('data-marker')].marker.setVisible(false);
      }
/*
      if(!arguments[2]) {
        $('#map-places li').eq(2).hide();
        //mapPlaces[$('#map-places li')[2].dataset.marker].marker.setVisible(false);
        mapPlaces[$('#map-places li')[2].getAttribute('data-marker')].marker.setVisible(false);
      }
*/
    } else {
      $('#map-places li').show();
      showAllMarkers();
    }
  }

  ManMap.reset = resetMap;

  ManMap.showLinks = showLinks;

}

function loadScript() {
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDQuBmx9sMJjva57fAFSPhPYuCIYRQ-NoM&v=3.9&sensor=false&' +
      'callback=initialize';
  document.body.appendChild(script);
}

function cancelEvent(ev) {
  ev.preventDefault();
  ev.stopPropagation();
  return false;
}
window.onload = loadScript;var ManPlayer = {};

var ManBrowser = {};

$(document).ready(function() {

	$('#browser-links').on('click', 'li', loadUrl);
	$('#video-links').on('click', 'li', loadVideo);

	function loadUrl(ev) {
		ev.preventDefault();
		$('#browser-links li').removeClass('active');
		$(this).addClass('active');

		//$('#browser-document').attr('src', this.dataset.url);
		$('#browser-document').attr('src', this.getAttribute('data-url'));

		/*$('#browser-document').on('load', function() {
			var doc = $(document.getElementById('browser-document').contentDocument);
			doc.find('a').on('click', function(ev) {
				ev.preventDefault();
				if(checkUrl(this.href)) {
					$('#browser-document').attr('src', this.href);
				} else {
					alert("Dieser Link ist gesperrt");
				}
			});
		});*/
	}

	function checkUrl(url) {
		var regxp = /(mandieselturbo.com|tecmania.ch|yousty.ch)/i;
		return url.search(regxp) !== -1;
	}

	function loadVideo(ev) {
		ev.preventDefault();
		$('#video-links li').removeClass('active');
		$(this).addClass('active');

		//$('#player').attr('src', 'videos/' + this.dataset.video);
		//$('#player').attr('src', 'videos/' + this.getAttribute('video'));
		videojs('player').src('videos/' + this.getAttribute('data-video'));

		window.setTimeout(function(){
			videojs('player').play();
		}, 300);
		//$('#player')[0].play();
	}

	ManPlayer.reset = function () {
		//$('#player').attr('src', 'videos/' + $('#video-links li')[0].dataset.video);
		//$('#player').attr('src', 'videos/' + $('#video-links li').data('video'));

		videojs('player').src('videos/' + $('#video-links li').data('video'));

		$('#video-links li').removeClass('active');
		$('#video-links li')[0].classList.add('active');
	};

	ManBrowser.reset = function () {
		//$('#browser-document').attr('src', $('#browser-links li')[0].dataset.url);
		$('#browser-document').attr('src', $('#browser-links li').data('url'));
		$('#browser-links li').removeClass('active');
		$('#browser-links li')[0].classList.add('active');
	};

	// wrapper_view_videos
	var video_player_container = document.querySelector('#wrapper_view_videos');

if(window.MutationObserver) {
	var DOMObserver = new MutationObserver(function(mutations) {
		mutations.forEach(function(mutation) {
		var display = $(mutation.target).css('display');
		if(display == 'block') {
			//$('#player')[0].play();
			videojs('player').play();
		} else if(display == 'none') {
			videojs('player').pause();
		}
	  });
	});
}

});

window.print = function(){};

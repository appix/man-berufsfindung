﻿
var mainNavi = 'home';
var view=-1;
var home=true;
var online = navigator.onLine;
$(document).ready(function(){
	$("body").on("touchmove", function(ev){
		event.preventDefault()

	});

	window.setInterval(function(){
		if(screen.deviceXDPI > 110 || screen.deviceXDPI < 90){
			document.location.reload();
		}
	}, 1000);


		var talentCheck = {"questions":	[
											{"question": "Ich bin …","minAnswer":2,maxAnswer:100, answers:	[
																					{image:1,"title":"kontaktfreudig","icon":"icon","points":	[
																																			{"jobs":[1,2,3]},
																																			{"jobs":[]},
																																			{"jobs":[0]}
																																		]
																					},
																					{image:2,"title":"kommunikativ","icon":"icon","points":		[
																																			{"jobs":[1,2,3]},
																																			{"jobs":[]},
																																			{"jobs":[0]}
																																		]
																					},
																					{image:3,"title":"gut organisiert","icon":"icon","points":		[
																																			{"jobs":[]},
																																			{"jobs":[]},
																																			{"jobs":[0,1,2,3]}
																																		]
																					},
																					{image:4,"title":"ruhig","icon":"icon","points":		[
																																			{"jobs":[2]},
																																			{"jobs":[]},
																																			{"jobs":[0,1,3]}
																																		]
																					},
																					{image:5,"title":"konzentriert","icon":"icon","points":		[
																																			{"jobs":[0,1,3]},
																																			{"jobs":[]},
																																			{"jobs":[2]}
																																		]
																					},
																					{image:16,"title":"manuell begabt","icon":"icon","points":	[
																																			{"jobs":[2]},
																																			{"jobs":[]},
																																			{"jobs":[1,3]}
																																		]
																					},
																					{image:7,"title":"fit","icon":"icon","points":		[
																																			{"jobs":[0,2]},
																																			{"jobs":[]},
																																			{"jobs":[3,1]}
																																		]
																					},
																					{image:8,"title":"teamorientiert","icon":"icon","points":		[
																																			{"jobs":[2]},
																																			{"jobs":[]},
																																			{"jobs":[0,1,3]}
																																		]
																					},
																					{image:9,"title":"logisch","icon":"icon","points":		[
																																			{"jobs":[2]},
																																			{"jobs":[]},
																																			{"jobs":[0,1,3]}
																																		]
																					},
																					{image:10,"title":"genau","icon":"icon","points":		[
																																			{"jobs":[]},
																																			{"jobs":[]},
																																			{"jobs":[0,1,2,3]}
																																		]
																					},

																				]
												},{"question": "In der Schule mag ich …","minAnswer":2,maxAnswer:100, answers:	[
																					{image:27,"title":"Deutsch","icon":"icon","points":	[
																																			{"jobs":[1,2,3]},
																																			{"jobs":[]},
																																			{"jobs":[0]}
																																		]
																					},
																					{image:28,"title":"Französisch","icon":"icon","points":		[
																																			{"jobs":[]},
																																			{"jobs":[]},
																																			{"jobs":[0]}
																																		]
																					},
																					{image:29,"title":"Englisch","icon":"icon","points":		[
																																			{"jobs":[0]},
																																			{"jobs":[]},
																																			{"jobs":[1,2,3]}
																																		]
																					},
																					{image:34,"title":"Arithmetik und Algebra","icon":"icon","points":		[
																																			{"jobs":[0]},
																																			{"jobs":[]},
																																			{"jobs":[3,1,2]}
																																		]
																					},
																					{image:37,"title":"Geometrie","icon":"icon","points":	[
																																			{"jobs":[]},
																																			{"jobs":[]},
																																			{"jobs":[3,1,2]}
																																		]
																					},
																					{image:16,"title":"Handarbeit nicht textil","icon":"icon","points":		[
																																			{"jobs":[2]},
																																			{"jobs":[]},
																																			{"jobs":[3,1]}
																																		]
																					},
																					{image:6,"title":"zeichnen","icon":"icon","points":	[
																																			{"jobs":[]},
																																			{"jobs":[]},
																																			{"jobs":[2]}
																																		]
																					},
																					{image:32,"title":"Sport","icon":"icon","points":	[
																																			{"jobs":[]},
																																			{"jobs":[]},
																																			{"jobs":[]}
																																		]
																					}

																				]
												},{"question": "Ich besuche …","minAnswer":1,maxAnswer:1, answers:	[
																					{"title":"Sek A","icon":"icon","points":	[
																																			{"jobs":[]},
																																			{"jobs":[]},
																																			{"jobs":[0,1,2,3]}
																																		]
																					},
																					{"title":"Sek B","icon":"icon","points":	[
																																			{"jobs":[]},
																																			{"jobs":[]},
																																			{"jobs":[1]}
																																		]
																					},
																					{"title":"BEZ/Gymnasium","icon":"icon","points":	[
																																			{"jobs":[]},
																																			{"jobs":[]},
																																			{"jobs":[0,1,2,3]}
																																		]
																					},
																					{"title":"10. Schuljahr","icon":"icon","points":	[
																																			{"jobs":[0,1,2,3]},
																																			{"jobs":[]},
																																			{"jobs":[]}
																																		]
																					},
																					{"title":"anderes","icon":"icon","points":	[
																																			{"jobs":[]},
																																			{"jobs":[]},
																																			{"jobs":[]}
																																		]
																					},
																				]
												},{"question": "Mich interessiert …","minAnswer":2,maxAnswer:100, answers:	[
																					{image:14,"title":"schreiben","icon":"icon","points":	[
																																			{"jobs":[]},
																																			{"jobs":[]},
																																			{"jobs":[0]}
																																		]
																					},
																					{image:15,"title":"Präsentation halten","icon":"icon","points":	[
																																			{"jobs":[]},
																																			{"jobs":[]},
																																			{"jobs":[0]}
																																		]
																					},
																					{image:13,"title":"fertigen/Maschine programmieren","icon":"icon","points":	[
																																			{"jobs":[]},
																																			{"jobs":[]},
																																			{"jobs":[3,1]}
																																		]
																					},
																					{image:16,"title":"montieren","icon":"icon","points":	[
																																			{"jobs":[]},
																																			{"jobs":[]},
																																			{"jobs":[3,1]}
																																		]
																					},
																					{image:17,"title":"zeichnen","icon":"icon","points":	[
																																			{"jobs":[]},
																																			{"jobs":[]},
																																			{"jobs":[2]}
																																		]
																					},
																					{image:18,"title":"berechnen","icon":"icon","points":	[
																																			{"jobs":[]},
																																			{"jobs":[]},
																																			{"jobs":[2]}
																																		]
																					}
																				]
												},{"question": "Mein zukünftiger Arbeitsplatz …","minAnswer":1,maxAnswer:1, answers:	[
																					{image:11,"title":"Schreibtisch mit Telefon und Computer","icon":"icon","points":	[
																																			{"jobs":[]},
																																			{"jobs":[]},
																																			{"jobs":[0]}
																																		]
																					},
																					{image:12,"title":"Schreibtisch mit <br>2 Bildschirmen","icon":"icon","points":	[
																																			{"jobs":[]},
																																			{"jobs":[]},
																																			{"jobs":[2]}
																																		]
																					},
																					{image:13,"title":"an der Maschine","icon":"icon","points":	[
																																			{"jobs":[]},
																																			{"jobs":[]},
																																			{"jobs":[3,1]}
																																		]
																					},
																				]
												},{"question": "Ich arbeite gerne mit …","minAnswer":2,maxAnswer:100, answers:	[
																					{image:20,"title":"PC","icon":"icon","points":	[
																																			{"jobs":[]},
																																			{"jobs":[]},
																																			{"jobs":[0,2]}
																																		]
																					},{image:21,"title":"Telefon","icon":"icon","points":	[
																																			{"jobs":[2]},
																																			{"jobs":[]},
																																			{"jobs":[0]}
																																		]
																					},{image:22,"title":"Akten","icon":"icon","points":	[
																																			{"jobs":[]},
																																			{"jobs":[]},
																																			{"jobs":[0]}
																																		]
																					},{image:14,"title":"Technische Zeichnung","icon":"icon","points":	[
																																			{"jobs":[]},
																																			{"jobs":[]},
																																			{"jobs":[2]}
																																		]
																					},{image:16,"title":"Werkzeug","icon":"icon","points":	[
																																			{"jobs":[]},
																																			{"jobs":[]},
																																			{"jobs":[3,1]}
																																		]
																					},{image:23,"title":"Kran","icon":"icon","points":	[
																																			{"jobs":[]},
																																			{"jobs":[]},
																																			{"jobs":[3,1]}
																																		]
																					},{image:24,"title":"Gabelstapler","icon":"icon","points":	[
																																			{"jobs":[]},
																																			{"jobs":[]},
																																			{"jobs":[3,1]}
																																		]
																					},{image:25,"title":"CAD-Programm","icon":"icon","points":	[
																																			{"jobs":[]},
																																			{"jobs":[]},
																																			{"jobs":[2]}
																																		]
																					}

																				]

												},
												{"question": "In 15 Jahren bin ich …","minAnswer":1,maxAnswer:100, answers:	[
																					{image:6,"title":"Ingenieur","icon":"icon","points":	[
																																			{"jobs":[]},
																																			{"jobs":[]},
																																			{"jobs":[2,3]}
																																		]
																					},{image:33,"title":"Auslandmonteur","icon":"icon","points":	[
																																			{"jobs":[]},
																																			{"jobs":[]},
																																			{"jobs":[3,1]}
																																		]
																					},{image:34,"title":"Finanzchef","icon":"icon","points":	[
																																			{"jobs":[]},
																																			{"jobs":[]},
																																			{"jobs":[0]}
																																		]
																					},{image:35,"title":"Projektleiter","icon":"icon","points":	[
																																			{"jobs":[]},
																																			{"jobs":[]},
																																			{"jobs":[0,2]}
																																		]
																					},{image:30,"title":"Werkstattleiter","icon":"icon","points":	[
																																			{"jobs":[]},
																																			{"jobs":[]},
																																			{"jobs":[3,1]}
																																		]
																					},{image:31,"title":"Personalleiter","icon":"icon","points":	[
																																			{"jobs":[]},
																																			{"jobs":[]},
																																			{"jobs":[0]}
																																		]
																					}

																				]
												},

										]};

		var varJobs =	[
							{img:"talent_kv.png",file:"job_kv.html","name": "Kaufleute", "points":0,max:62},
							{img:"talent_produktion.png",file:"job_produktionsmech.html","name": "Produktionsmechaniker","points":0,max:68},
							{img:"talent_kon.png",file:"job_konstrukteur.html","name": "Konstrukteur","points":0,max:61},
							{img:"talent_poly.png",file:"job_poly.html","name": "Polymechaniker","points":0,max:68}
							/*,
							{img:"talent_anlage.png",file:"job_apparate.html","name": "Anlagen- & Apparatebauer","points":0,max:68},
							{img:"talent_inf.png",file:"job_informatiker.html","name": "Informatiker","points":0,max:50}*/
						];

		var myScroll = new iScroll('wrapper_mainNavi', {
			snap: false,
			momentum: true,
			hScrollbar: false,
			useTransition:false,
			zoom: false
		 });

		var myScroll_job = 0;
		var myScroll_gallery = 0;




		var myScroll_about= new iScroll('wrapper_view_about', {
			snap: false,
			momentum: true,
			hScrollbar: false,
			useTransition:false,
			zoom: false,
			onScrollMove: function() {
				isScrolling = true;
			},
			onScrollEnd: function() {
				isScrolling = false;
			}
		 });

		var myScroll_jobs = new iScroll('wrapper_view_jobs', {
			snap: false,
			momentum: true,
			hScrollbar: false,
			useTransition:false,
			zoom: false,
			onScrollMove: function() {
				isScrolling = true;
			},
			onScrollEnd: function() {
				isScrolling = false;
			}
		 });


		 var myData = 0;
		 var navi = {main:["jobs", "about", "talent"],jobs:["a","b"]};
		 view=0;
		 var lock = 0;
		 mainNavi = 'jobs';
		 home=true;



		var actQuestion=-1;

		 //Main Navigation

		 $("#header_view_navi").on("click", function(ev){

		 	if(lock!=1){
				var tmpNavi = $(ev.target).attr("data-navi");
				if( tmpNavi == 'home'){
					if(view!=0){
						$('#scroller_navi li[data-navi="jobs"]').trigger("click");
					}
				} else {
					if(view!=0){
						$('#scroller_navi li[data-navi="' + tmpNavi + '"]').trigger("click");
						$(".jobAnimation").on("transitionend", function(){
							$(".jobAnimation").unbind("transitionend");
							$('#scroller_navi li[data-navi="' + tmpNavi + '"]').trigger("click");
						});
					} else {
						$('#scroller_navi li[data-navi="' + tmpNavi + '"]').trigger("click");
					}
				}
			}
		});

		 $("#scroller_navi li").bind("click", function(){

		 	if(lock!=1){
		 		lock=1;
				 var page = $(this).attr("data-navi");

			 	switch(view){
			 		case(1):


						if(mainNavi == 'talent'){
							$("#main_view_2").unbind("click");
							$("#main_view_1").unbind("click");
						}
				 		view=0;
						$("#header_view_navi div").removeClass('active');
						$("#header_view_navi div:nth-child(1)").addClass('active');
				 		helperActScrollJob=0;
						//$("#wrapper_mainNavi").css("background-position", "0%,0%")
				 		myScroll.enable();
				 		$("#scroller_navi").css("width", "100vw");
					 	myScroll.refresh();
					 	$("#wrapper_mainNavi").css("transform", "translate(0px)")
						myScroll.scrollTo(0,0,400);
					 	$("#scroller_navi li div:nth-child(1)").css("transition-delay", "0s");
					 	$("#scroller_navi li div:nth-child(1)").css("transform","translateX(0%)");
					 	$("#main_view_1").css("transform","translateX(110%)");
					 	$("#main_view_3").css("transform","translateX(150%)");
					 	$("#wrapper_view_html").html(" ");
					 	$("#main_view_1").on("transitionend", function(){
					 		$("#main_view_1").unbind("transitionend");
							$(".about").hide();
							$(".talent").hide();

					 		lock=0;
					 	});

						myScroll_about.scrollTo(0,0,400);

						break;

				 	case(0):

				 		mainNavi = $(this).attr("data-navi");
						$("#header_view_navi div").removeClass('active')
						$('#header_view_navi div[data-navi="' + mainNavi + '"]').addClass('active')
						if(mainNavi == 'talent'){
							$(".sniff").unbind("click");
							$(".sniff").on("click", function(){
								$(".sniff").unbind("click");

								showSniff($(this));
							});

							actQuestion=-1;
							talentHandler();
						}

						helperActScrollJob=0;
						$('.talent, .jobs, .about').hide();
						$('.' + mainNavi).show();
					 	view=1;
						var scrollTo = Math.floor($(this).position().left);
						var scrollTo = $(this).prevAll().get().reduce(function (width, el) {
							return width + $(el).width();
						}, 0);

					 	$("#scroller_navi").css("width", "200%");
						myScroll.refresh();



					 	//myScroll.scrollTo(100,0,400);
					 	myScroll.scrollTo(-1*scrollTo,0,700);

					 	//$("#wrapper_mainNavi").css("transform", "translate("+(-scrollTo)+"px)")


						$("#scroller_navi li div:nth-child(1)").css("transition-delay", "0.3s");

						$("#scroller_navi li div:nth-child(1)").css("transform","translateX(-65%)");
					  	//$("#main_view_1 ul img").css("opacity", "0.5");
					 	$("#main_view_1").css("transform","translateX(16.7%)");
					 	$("#main_view_1").on("transitionend", function(){
					 		$("#main_view_1").unbind("transitionend");
					 		//$("#wrapper_mainNavi").css("background-position", "500px,0%")
						if(mainNavi == 'about'){

							myScroll_about.refresh();

				 		}
					 		lock = 0;
					 		myScroll.disable();
					 	});
					 	myScroll_jobs.refresh();

				 	break;
				 	case(2):

						ManForm.reset();
		  				ManPlayer.reset();
						if(online!=false)
		  					ManMap.reset();
		  				ManBrowser.reset();
				 		lock = 0;
				 		helperActScrollJob=0;
				 		helperActScrollAbout=0;

				 		myScroll_jobs.scrollTo(1,0,400);


						if(mainNavi == 'jobs'){
				 			$("#scroller_view_jobs li:nth-child(1)").trigger("click");
				 		}else{
				 			$("#scroller_view_about li").first().trigger("click");
				 		}

				 		lock = 0;
				 		$("#scroller_navi li:nth-child(1)").trigger("click");
				 	break;
				 }
		 	}else{
		 		//alert("fdsf");
		 	}




		 });

		 //Navigation Jobs

		 var helperActScrollJob=0;
		 var helperActScrollAbout=0;
		 var isScrolling;


		  $("#scroller_view_jobs li, #scroller_view_about li").bind("click", function(){

		  if(isScrolling) {
			return;
		  }

		 	if(lock!=1){
		 		lock=1;
				var page = $(this).attr("data-navi");

			  	if(view==2){
			  		view=1;

			 		//alert("fsd");
					//$("#main_view_2").css("transition-duration", "1.1s")
					//$("#main_view_1").css("transform","translateX(16.3333%)");

					if(mainNavi == 'jobs'){


			 			$("#main_view_3").css("transform","translateX(150%)");

			 			$("#main_view_2").trigger("click");

						/*
						$("#job_second_wrapper").css("transition-duration","1.4s");
						$("#job_second_wrapper").css("transform","translateX(200%)");
						/*
						 *
						 */

						$("#job_second_wrapper").hide();

			 			$("#job_first_wrapper").css("transition-duration","2.2s");
						$("#job_first_wrapper").css("transform","translateX(200%)");



							$("#job_second_wrapper").unbind("transitionend");
							$("#main_view_1").css("transform","translateX(16.3333%)");
							$("#scroller_view_jobs ul").css("transform", "translate(0px,0px)");

							$("#main_view_2").css("transition-delay", "0.2s")
							$("#main_view_2").css("transform","translateX(150%)");

						/*
							$("#job_first_wrapper").on("transitionend", function(){
								alert("test");
								$("#job_second_wrapper").unbind("transitionend");
								$("#main_view_1").css("transform","translateX(16.3333%)");
								$("#scroller_view_jobs ul").css("transform", "translate(0px,0px)");

								$("#main_view_2").css("transition-delay", "0.2s")
								$("#main_view_2").css("transform","translateX(150%)");
							});



						"/"
						/*
						$("#job_second_wrapper").on("transitionend", function(){


						});
							*/
			 			//


					 	$("#main_view_2").on("transitionend", function(){
					 		$("#main_view_2").unbind("transitionend");


					 		$("#main_view_2").html("");
					 		lock=0;
					 	});
					 	myScroll_jobs.enable();
			 		}else{

			 			$("#main_view_1").css("transform","translateX(16.3333%)");
			 			//About
			 			$("#scroller_view_about ul").css("transform", "translate(0px,0px)");
					 	$("#main_view_2").on("transitionend", function(){
					 		$("#main_view_2").unbind("transitionend");
					 		/*
					 		myScroll_j.destroy();
					 		myScroll_gallery.destroy();
					 		*/
					 		$("#main_view_2").html("");
					 		lock=0;
					 	});


					 	myScroll_about.enable();

					 	if($(this).attr("class")!="notdirect"){
					 		$("#main_view_2").css("transform","translateX(150%)");
				  			$("#main_view_2").on("transitionend", function(){
				  				$("#main_view_2").html(" ");
				  				$("#main_view_2").unbind("transitionend");
				  				lock=0;

				  			});
				  		}else{

				  			$("#main_view_3").css("transform","translateX(150%)");

				  			$("#main_view_3").on("transitionend", function(){

				  				$("#wrapper_view_html").html(" ");
				  				ManForm.reset();
				  				ManPlayer.reset();
								if(online!=false)
									ManMap.reset();
				  				ManBrowser.reset();
				  				$("#main_view_3").unbind("transitionend");
				  				$("#main_view_2").html(" ");
				  				lock=0;
				  			});


				  		}

			 		}











			  	}else{

			  		view=2
					iscrollPage = parseInt(page)+1;
					$("#main_view_2").html(" ");
					$("#main_view_2").css("transition-duration", "0.7s");
			 		if($(this).attr("class")!="notdirect"){
						$.ajax({
							url:'data/'+$(this).attr('data-page'),
							success: function(data){
								myData = data;
							},
							async: false
						});
				 		//$.get("data/"+$(this).attr("data-page"), function(data){
				 		//	myData = data;
					 	//});
					 }

					//Jobs
					if(mainNavi == 'jobs'){
						$("#scroller_view_jobs ul").css("transition-duration", "1s")
						myScroll_jobs.disable();
						matrix = matrixToArray($("#scroller_view_jobs").css("transform"));
						helperActScrollJob = matrix[4];
						$("#scroller_view_jobs ul").css("transform", "translate("+((($(this).width())*-page)-helperActScrollJob-10)+"px,0px)");


					 	$("#scroller_view_jobs ul").on("transitionend", function(){
					 		$("#scroller_view_jobs ul").unbind("transitionend");
					 		//alert(myData);
					 		$("#main_view_2").html(myData);

							$(".jobs_icon_maps").on("click", function(){
								$(".jobs_icon_maps").unbind("click");
								showSniff($(this));
							});

				 			$(".sniff").unbind("click");
							$(".sniff").on("click", function(){
								$(".sniff").unbind("click");
								showSniff($(this));
							});
							/*

							myScroll_job = new iScroll('job_first_wrapper', {
								snap: false,
								momentum: true,
								hScrollbar: false,
								useTransition:false,
								zoom: false
							 });

							myScroll_gallery = new iScroll('job_second_wrapper', {
								snap: true,
								momentum: false,
								hScrollbar: false,
								useTransition:false,
								zoom: false,
								onScrollEnd: function () {
									document.querySelector('#indicator > li.active').className = '';
									document.querySelector('#indicator > li:nth-child(' + (this.currPageX+1) + ')').className = 'active';
								}
							 });

							 */
					 		lock=0;
					 	});



					}else{
						//About

						$("#scroller_view_about ul").css("transition-duration", "1s")
						myScroll_about.disable();
						matrix = matrixToArray($("#scroller_view_about").css("transform"));
						helperActScrollAbout = matrix[4];
						$("#scroller_view_about ul").css("transform", "translate("+((($(this).width())*-page)-helperActScrollAbout-10)+"px,0px)");


					 	$("#scroller_view_about ul").on("transitionend", function(){
					 		$("#main_view_2").html(myData);
					 		$("#scroller_view_about ul").unbind("transitionend");
					 		//alert(myData);


							/* TEST_1
							alert("test");

					 		$("#main_view_2").html(myData);
				 			$(".sniff").unbind("click");
							$(".sniff").on("click", function(){
								$(".sniff").unbind("click");
								alert("this one");
								showSniff($(this));
							});


							*/
							/*
							myScroll_about = new iScroll('about_fist_wrapper', {
								snap: false,
								momentum: true,
								hScrollbar: false,
								useTransition:false,
								zoom: false
							 });

							myScroll_gallery = new iScroll('job_second_wrapper', {
								snap: true,
								momentum: false,
								hScrollbar: false,
								useTransition:false,
								zoom: false,
								onScrollEnd: function () {
									document.querySelector('#indicator > li.active').className = '';
									document.querySelector('#indicator > li:nth-child(' + (this.currPageX+1) + ')').className = 'active';
								}
							 });
							 */
					 		lock=0;
					 	});
					}

				 	$("#main_view_2").css("transition-delay", "0s");
				 	$("#main_view_1").css("transform","translateX(8.3333%)");
				 	if($(this).attr("class")!="notdirect"){
				 		lock=1;
				 		$("#main_view_2").css("transform","translateX(40%)");
			  		}else{
			  			//alert($(this).attr("data-page"));
						$("#main_view_3 .sub").hide();
						$("#main_view_3 #"+$(this).attr("data-page")).show();
			  			$("#main_view_3").css("transform","translateX(40%)");
			  			lock=1;
			  		}


			  		//alert($("#scroller_view_jobs li:nth-child(1)").width()
				 	//var scrollTo = (page)*(($(window).width())/6);




				 	//$("#scroller_view_jobs").css("width", "200%");
				 	//myScroll_jobs.refresh();
				 	//myScroll_jobs.scrollTo(10,0,0);

				 	//alert(iscrollPage);
				 	//myScroll_jobs.refresh();
				 	//myScroll_jobs.scrollToElement('li:nth-child('+(iscrollPage)+')', 300)
				 	//At Work
				 	//console.log(($(this).width()+0.5));


				 	//alert(myData);


				 	//alert(myData);



				 	/*
				 	$("#scroller_view_jobs li:nth-child("+iscrollPage+") div.blueTitleBox").css("padding-bottom", "80%");



				 	$("#scroller_view_jobs li").on("transitionend", function(){
				 		$("#scroller_view_jobs li").unbind("transitionend");
				 		$("#scroller_view_jobs li").css("transition-delay", "0s");

				 	});

				 	$("#scroller_view_jobs li:nth-child("+iscrollPage+") div.textRotate").css("transform", "rotate(-90deg)");

				 	*/
				 	//$("#scroller_view_jobs li:nth-child("+iscrollPage+") div span").css("transform", "rotate(90deg)");


			  	}
			  }else{

			  }

		  });

		 function matrixToArray(matrix) {
			if(matrix.substr(6, 2) == '3d') {
				var matrix_3d = matrix.substr(9, matrix.length - 8).split(', ');
				return [matrix_3d[0], matrix_3d[1], matrix_3d[4], matrix_3d[5], matrix_3d[12], matrix_3d[13]];
			} else {
				return matrix.substr(7, matrix.length - 8).split(', ');
			}
		}
		 window.setTimeout(function(){
		 	myScroll.refresh();
		 },300);

		// myScroll.refresh();



		 //Talent Check
		 //var givenAnswer = 100;
		 $("#question_next").on("click", function(){
		 	if(givenAnswer<=0){
		 		givenAnswer=1;
			 	actQuestion++;
			 	if(actQuestion<talentCheck.questions.length)
			 		$("#question_xy_value").text(actQuestion+1);

			 	talentHandler();
			 }
		 });

		function talentHandler(){

			if(actQuestion==-1){
				resetTalentCheck();
			}else{
				setQuestion();

			}

		}

		function setQuestion(){


			if(actQuestion!=0){

				$("#question_text").addClass("question_text_ani_end");
				$("#question_text").on("animationend", function(){
					$(this).unbind("animationend");

				});

				$("#talent_answer div.answer_button:nth-child(3n-1)").on("animationend", function(){
					$("#talent_answer div.answer_button:nth-child(3n-1)").unbind("animationend");
					if(actQuestion<talentCheck.questions.length){
						showQuestion();
						if(talentCheck.questions[actQuestion].minAnswer==1)
							$("#textAnswer").text("Antwort");
						else
							$("#textAnswer").text("Antworten");


						if(actQuestion+1==talentCheck.questions.length)
							$("#question_next").text("Zur Auswertung");
					}else{

						$("#state_view").on("transitionend", function(){
							$("#state_view").unbind("transitionend");
							$("#state_view").hide();
							$("#state_result_view").css("transform", "translateX(0%)");
						});
						$("#state_view").css("transform", "translateX(-100%)");
						showResult();

					}


				});
			$("#question_next").css("opacity", "0.4");
			$("#question_next").css("color", "#c5c5c5")
			$(".answer_button").addClass("answer_button_out");
			window.setTimeout(function(){
				$(".answer_button").trigger('animationend');
			}, 1000);

			}else{

			$("#question_next").css("opacity", "0.4");
			$("#question_next").css("color", "#c5c5c5")
			$(".answer_button").addClass("answer_button_out");
				showQuestion();
			}


		//alert(.minAnswer);
		}

		var arrayHelper = [];
		function showResult(){


			$("#talent_answer").empty();
			helper=0;
			maxTmp = 0;
			max = 0;
			$.each(varJobs, function(key,value){
				helper=$("<div class='spacer_talent_result'> </div><div class='resultSet'><div class='percentage'><img class='sniff' data-file='"+value.file+"' data-page='wrapper_view_html' src='img/"+value.img+"'></div><div class='jobName'><b>"+Math.round((value.points/value.max)*100)+"% "+value.name+"</div></div>");
				$("#talent_answer").append(helper);

				help = {markup:$(helper).find(".percentage"), width:Math.round((value.points/value.max)*100), name:value.name}

				if(Math.round((value.points/value.max)*100)>maxTmp){
					max = help;
					maxTmp = max.width;
				}


				arrayHelper.push(help);
	 			$(".sniff").unbind("click");
				$(".sniff").on("click", function(){
					$(".sniff").unbind("click");
					showSniff($(this));
				});
				//$(helper).find(".percentage").css("width",Math.round((value.points/value.max)*100)+"%");

			});
			/*
			$(".percentage img").bind("click", function(){
				$("#main_view_3").css("transform","translateX(40%)");
				$("#main_view_3 .sub").hide();
				$("#main_view_3 #wrapper_view_html").show();
			});
			*/

			$("#talent_percent").text(max.width+"%");
			$("#talent_jobTitle").text(max.name);

			window.setTimeout(function(){
				animateTalentResult();
			},100);

		}

		function animateTalentResult(){
			$("#question_text").text("Dein Ergebnis!");
			$("#question_xy").css("opacity", 0)
			$("#question_text").removeClass("question_text_ani_end");
			$("#question_text").addClass("question_text_ani");

			$.each(arrayHelper, function(key,value){
				value.markup.css("transition", "width "+2/((100-value.width)/100)+"s");
				$(value.markup).css("width",value.width+"%");
			});


		}

		function showQuestion(){
			var countAnswer = 0;
			$("#question_x_value").text(talentCheck.questions[actQuestion].minAnswer);



			$("#question_text").text(talentCheck.questions[actQuestion].question);
			$("#question_text").removeClass("question_text_ani_end");
			$("#question_text").addClass("question_text_ani");
			$("#question_text").on("animationend", function(){
				$("#question_text").unbind("animationend");

				$("#question_text").removeClass("question_text_ani");
			});


			$("#talent_answer").empty();


			$.each(talentCheck.questions[actQuestion].answers, function(key, value) {
			  	 helper = $('<div active="0" answerId="'+key+'" class="answer_button"><div class="answer_box"><div class="answer_text">'+value.title+'</div><div class="answer_icon" style="background-image:url(\'img/talent_icon/'+value.image+'_a.svg\')"><br><br><br></div></div></div>');
				// helper.css("animation-delay",Math.random() * (0.5)+"s");
				 $("#talent_answer").append(helper);

			});

			$(".answer_button").on("click", function(){

				if($(this).attr("active")==0){


					if(countAnswer<talentCheck.questions[actQuestion].maxAnswer){


						countAnswer++;
						$(this).attr("active", 1)
						$(this).addClass("answer_button_active");
						$.each(talentCheck.questions[actQuestion].answers[$(this).attr("answerId")].points,function(index,value){
							$.each(value,function(key,value){
								$.each(value,function(key,value){
									varJobs[value].points+=index+1;
								});

							});
						});
					}
				}else{
					countAnswer--;
					$(this).attr("active", 0)
					$(this).removeClass("answer_button_active");
					$.each(talentCheck.questions[actQuestion].answers[$(this).attr("answerId")].points,function(index,value){
						$.each(value,function(key,value){
							$.each(value,function(key,value){
								varJobs[value].points-=index+1;
							});

						});
					});


				}
				givenAnswer = (talentCheck.questions[actQuestion].minAnswer-countAnswer);
				//minAnswers = talentCheck.questions[actQuestion].minAnswer-countAnswer;
				//alert("dsa: "+minAnswer);
				//talentCheck.questions[actQuestion].minAnswer
				if(givenAnswer<=0){
					$("#question_x_value").text(0);
					$("#textAnswer").text("Antworten");
					$("#question_next").css("opacity", "1")
					$("#question_next").css("color", "#e20046")
				}else{
					$("#question_x_value").text(givenAnswer);
					if(givenAnswer==1)
						$("#textAnswer").text("Antwort");
					else
						$("#textAnswer").text("Antworten");

					$("#question_next").css("opacity", "0.4")
					$("#question_next").css("color", "#c5c5c5")
				}
			});

		}

		function resetTalentCheck(){
				$("#question_xy").css("opacity", 1)
				$("#question_x_value").text("");
				$("#question_next").text("Nächste Frage");
				$("#textAnswer").text("Antworten");
				$("#talent_answer").text("");
				$("#question_text").text("");
				$("#question_xy_value").text(1);
				$("#state_view").show();
				$("#state_view").css("transform", "translateX(0%)");
				$("#state_result_view").css("transform", "tranlateX(0%)");
				$.each(varJobs, function(key,value){
					value.points = 0;
				});
				actQuestion=0;
				talentHandler();
		}


		//Schnuppern

		$(".sniff").on("click", function(){
			showSniff($(this));
		});




		function showSniff(attr){
			$("#job_second_wrapper").hide();
			$("#main_view_3 .sub").hide();
			$("#main_view_3 #"+$(attr).attr("data-page")).show();
			if($(attr).attr("data-page")=="wrapper_view_html"){
				$("#wrapper_view_html").html(" ");

		 		//$.get("data/"+$(attr).attr("data-file"), function(data){
		 		//	myData = data;
		 		//	//$("#wrapper_view_html").html(myData);
			 	//});
				$.ajax({
					url:'data/'+$(attr).attr('data-file'),
					success: function(data){
						myData = data;
					},
					async: false
				});
			}
			$("#main_view_3").css("transform","translateX(40%)");

			if(view==2){

				if($(attr).attr("data-page")=="wrapper_view_html"){
					$("#main_view_3").on("transitionend", function(){
						$("#main_view_3").unbind("transitionend");
						$("#wrapper_view_html").html(myData);
					});
				}
				$("#main_view_2").css("transform","translateX(25%)");
				$("#main_view_2").on("transitionend", function(){
					$("#main_view_2").unbind("transitionend");
					$("#main_view_2").on("click", function(){
						$("#main_view_2").unbind("click");

						$("."+$(attr).attr("class")).on("click", function(){

							$("."+$(attr).attr("class")).unbind("click");

							showSniff($(this));
						});
						$("#main_view_2").css("transform","translateX(40%)");

						$("#main_view_3").css("transform","translateX(150%)");
						$("#main_view_3").on("transitionend", function(){
							$("#main_view_3").unbind("transitionend");

								ManForm.reset();
				  				ManPlayer.reset();
								//console.log(ManMap, ManMap.reset, !!ManMap.reset);
								if(online!=false)
				  					ManMap.reset();
				  				ManBrowser.reset();
							$("#wrapper_view_html").html(" ");
							//ManForm.reset();
							$("#job_second_wrapper").show();
						});

					});
				});
			}else{
				if($(attr).attr("data-page")=="wrapper_view_html"){
					$("#main_view_3").on("transitionend", function(){
						$("#main_view_3").unbind("transitionend");
						$("#wrapper_view_html").html(myData);
					});
				}


				$("#main_view_1").css("transform","translateX(8.3333%)");
				$("#main_view_1").on("transitionend", function(){

					$("#main_view_1").unbind("transitionend");
					$("#main_view_1").on("click", function(){
						$("#main_view_1").unbind("click");

						$(".sniff").unbind("click");
						$(".sniff").on("click", function(){
							$(".sniff").unbind("click");

							showSniff($(this));
						});


						$("#main_view_1").css("transform","translateX(16.666%)");

						$("#main_view_3").css("transform","translateX(150%)");

					});
				});
			}
		}

});

var TouchKeyboard = (function () {

	var ks,
		activeInput,
		keyboardElement,
		keyShift = false,
		keyboardFragment = document.createDocumentFragment(),
		specialKeys = {
			//tab: ['?'],
			space: [' ', spaceFunc],
			//shift: ['?', shiftFunc],
			//enter: ['?'],
			//backspace: ['?', backspaceFunc],
			spacer: [''],

			// tab: [String.fromCharCode(8633)],
			// shift: [String.fromCharCode(8679), shiftFunc],
			// enter: [String.fromCharCode(8629)],
			// backspace: [String.fromCharCode(8592), backspaceFunc],
			
			tab: ['\u21B9'],
			shift: ['\u21E7', shiftFunc],
			enter: ['\u21B5'],
			backspace: ['\u2190', backspaceFunc],
		};

	var keyboarLayouts = {
		standard: 
			[
				[1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 'backspace'],
				['q', 'w', 'e', 'r', 't', 'z', 'u', 'i', 'o', 'p', '�'],
				['a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', '�', '�'],
				['shift', 'y', 'x', 'c', 'v', 'b', 'n', 'm', ',', '.'],
				['space']
			],
		email:
			[
				['q', 'w', 'e', 'r', 't', 'z', 'u', 'i', 'o', 'p', '�'],
				['a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', '�', '�'],
				['y', 'x', 'c', 'v', 'b', 'n', 'm', '.', '-', '@', '.ch']
			]
	};

	document.addEventListener('DOMContentLoaded', init, false);
	document.addEventListener('focus', setActiveFormElement, true);

	function setActiveFormElement(ev) { 
		if(ev.target.nodeName == 'INPUT') {
			activeInput = ev.target;
		}
	}

	function init() {
		keyboardElement = document.getElementById('keyboard');
		keyboardElement.addEventListener('touchstart', lockEvent, false);
		keyboardElement.addEventListener('click', lockEvent, false);
		keyboardElement.addEventListener('touchend', keyhit, false);
	}

	function keyhit(ev) {
		if(ev.target.nodeName === 'LI') {
			ev.preventDefault();
			var pressedKey = ev.target;
			if(pressedKey.dataset.letter) {
				ev.preventDefault();
				addChar(pressedKey.dataset.letter);
				if(keyShift) shiftFunc();
			} else {
				specialKeys[pressedKey.dataset.key][1]();
			}
		}
	}

	function addChar(c) {
		activeInput.focus();
		var val = activeInput.value;
		var newVarA = val.slice(0, activeInput.selectionStart);
		var newVarB = val.slice(activeInput.selectionEnd);
		activeInput.value = newVarA + c + newVarB;
		var position = newVarA.length + 1;
		activeInput.setSelectionRange(position, position);
		if(activeInput.selectionEnd <= position) {
			activeInput.scrollLeft = activeInput.scrollLeft + 4;
		}
		
	}

	function spaceFunc() {
		addChar(' ');
	}

	function lockEvent(ev) {
		ev.preventDefault();
		ev.stopPropagation();
	}

	function backspaceFunc() {
		activeInput.focus();
		var val = activeInput.value;
		var cut = activeInput.selectionStart;
		if(cut === activeInput.selectionEnd) cut--;
		var newVarA = val.slice(0, cut);
		var newVarB = val.slice(activeInput.selectionEnd);
		activeInput.value = newVarA + newVarB;
		var position = newVarA.length;
		activeInput.setSelectionRange(position, position);
		
	}

	function shiftFunc() {
		activeInput.focus();
		keyShift = !keyShift;
		console.log(keyShift);
		for(i = 0, ilen = ks.length; i < ilen; i++) {
			var kr = ks[i];
			for(j = 0, jlen = kr.length; j < jlen; j++) {
				var sk = specialKeys[kr[j]];
				if(sk) {
				} else {
					if(typeof kr[j] === 'string') {
						if(keyShift) {
							kr[j] = kr[j].toUpperCase();
						} else {
							kr[j] = kr[j].toLowerCase();
						}
					}
				}		
			}
		}
		render();
	}


	function render() {

		var kbrd = document.createElement('ol');

		for(i = 0, ilen = ks.length; i < ilen; i++) {
			var kr = ks[i];
			var rli = document.createElement('li');
			var row = document.createElement('ol');
			rli.appendChild(row);

			for(j = 0, jlen = kr.length; j < jlen; j++) {
				var item = document.createElement('li');
				var keyName = kr[j];
				var sk = specialKeys[keyName];
				if(sk) {
					item.dataset.key = keyName;
					item.appendChild(document.createTextNode(sk[0]));
					if(keyName === 'shift') {
						if(keyShift) item.classList.add('active');
					}
				} else {
					item.dataset.letter = keyName;
					item.appendChild(document.createTextNode(keyName));
				}		
				
				row.appendChild(item);
			}

			kbrd.appendChild(rli);
		}

		keyboardFragment.appendChild(kbrd);

		if(keyboardElement.hasChildNodes()) {
			keyboardElement.replaceChild(keyboardFragment, keyboardElement.children[0]);
		} else {
			keyboardElement.appendChild(keyboardFragment);
		}	
	}

	function setLayout(keys) {
		ks = keys;
		render();
	}

	return {
		setLayout: function (keys) {
			if(typeof keys === 'string') {
				setLayout(keyboarLayouts[keys]);
			} else {
				setLayout(keys);
			}
		}
		

	};

})();

document.addEventListener('focus', loadKeyboard, true);
document.addEventListener('blur', closeKeyboard, true);

function loadKeyboard(ev) {
	if(ev.target.form){
		var elm = ev.target;
		var showKeyboard = true;
		switch(elm.type) {
			case 'email':
				TouchKeyboard.setLayout('email');
				break;
			/*case 'number':
				TouchKeyboard.setLayout([[7,8,9],[4,5,6],[1,2,3]]);
				break;*/
			case 'checkbox':
			case 'radio':
				showKeyboard = false;
				break;
			default:
				TouchKeyboard.setLayout('standard');
		}
		if(showKeyboard) document.getElementById('keyboard').classList.add('opened');
	}
}

function closeKeyboard(ev) {
	if(document.activeElement == document.body) {
		document.getElementById('keyboard').classList.remove('opened');
	}
}
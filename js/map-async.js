var ManMap = {};

function initialize() {
  var mapCenter = new google.maps.LatLng(47.416821,8.628702);
  var mapZoom = 11;
  var mapOptions = {
    zoom: mapZoom,
    minZoom: 8,
    center: mapCenter,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    streetViewControl: false,
    draggable: false,
    panControl: false,
    zoomControl: false,
    mapTypeControl: false,
    scrollwheel: false,
    disableDoubleClickZoom: true
  };

  var map = new google.maps.Map(document.getElementById('map-canvas'),
      mapOptions);

  $('#map-places').on('click', 'li', toggleMarkerInfo);

  var mapPlaces = {
    man: {
      title: 'man',
      content_title: "MAN Energy Solutions Schweiz AG",
      position: new google.maps.LatLng(47.390656, 8.521732),
      address:"Hardstrasse 319, 8005 Zürich"
    },

    azw: {
      title: 'azw',
      content_title: "azw Ausbildungszentrum Winterthur",
      position: new google.maps.LatLng(47.497635, 8.716978),
      address: "Zürcherstrasse 25, 8401 Winterthur"
    }
  };

  var infowindow = null;

  for(var prop in mapPlaces) {
    if(mapPlaces.hasOwnProperty(prop)) {
      var marker_data = mapPlaces[prop];

      marker_data.marker = new google.maps.Marker({
        position: marker_data.position,
        map: map,
        title: marker_data.title
      });

      marker_data.infowindow = new google.maps.InfoWindow({
        content: '<div id="content">'+
            '<div id="siteNotice">'+
            '</div>'+
            '<h1 id="firstHeading" class="firstHeading">'+marker_data.content_title+'</h1>'+
            '<div id="bodyContent">'+
            '<address>'+marker_data.address+'</address>'+
            '</div>'+
            '</div>',
        maxWidth: 200
      });

      google.maps.event.addListener(marker_data.marker, 'click', toggleInfo);

    }
  }

  function toggleInfo() {
    if(infowindow) {
      $('li[data-marker]').removeClass('active');
      infowindow.close();
    }
    infowindow = mapPlaces[this.title].infowindow;
    infowindow.open(map,mapPlaces[this.title].marker);
    console.log('li[data-marker="' + this.title + '"]');
    $('li[data-marker="' + this.title + '"]').addClass('active');
    google.maps.event.addListenerOnce(infowindow, 'closeclick', resetWithoutClose);
    map.panTo(mapPlaces[this.title].position);
    map.setZoom(13);

  }

  function toggleMarkerInfo(ev) {
    if($(this).hasClass('active')) {
      reset();
    } else {
      if(this.dataset.marker) {
        toggleInfo.call(mapPlaces[this.dataset.marker]);
      }
    }
  }

  function reset() {
    map.setCenter(mapCenter);
    map.setZoom(mapZoom);
    if(infowindow) {
      $('li[data-marker]').removeClass('active');
      infowindow.close();
    }
  }

  function resetWithoutClose() {
    map.setCenter(mapCenter);
    map.setZoom(mapZoom);
    $('li[data-marker]').removeClass('active');
  }

  ManMap.reset = reset;

}

function loadScript() {
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src = 'https://maps.googleapis.com/maps/api/js?v=3.9&sensor=false&' +
      'callback=initialize';
  document.body.appendChild(script);
}

function cancelEvent(ev) {
  ev.preventDefault();
  ev.stopPropagation();
  return false;
}

window.onload = loadScript;

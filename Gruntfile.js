module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
	autoprefixer: {
		options: {
		  // Task-specific options go here.
		},
		multiple_files: {
			expand: true,
			flatten: true,
			src: 'css_orig/*.css',
			dest: 'css'
		}
	},
  });

  //grunt.loadNpmTasks('grunt-contrib-concat');
  // Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-autoprefixer');

  // Default task(s).
  grunt.registerTask('default', ['autoprefixer']);

};
# MAN Berufsfindung App

## Usage
To work properly the application files should be served by a local server like
xampp or server2go

## Missing Files
Video files, due to their large file size, are not saved in the repo.
They can be downloaded here:
https://drive.google.com/drive/folders/1_6o2EAZiP5_OgxHT6CH635quNKNX1iLb

They should be placed in the `video` folder.
